FROM openjdk:20-jdk-slim

COPY target/server-*.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","app.jar"]