package com.crcl.server.post.controller;

import com.crcl.server.post.domain.Post;
import com.crcl.server.post.service.PostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Random;

@RestController
@RequestMapping("/api/v1/posts")
public class PostController {

    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping
    public Mono<ResponseEntity<Post>> createPost(@RequestBody Post post) {
        return postService.createPost(post)
                .map(savedPost -> ResponseEntity.status(HttpStatus.CREATED).body(savedPost));
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Post>> getPostById(@PathVariable String id) {
        return postService.getPostById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping
    public Mono<Page<Post>> getAllPosts(Pageable pageable) {
        final Random random = new Random();
        final int delayInMillis = random.nextInt(2000);
        return postService.getAllPosts(pageable)
                .delayElement(Duration.ofMillis(delayInMillis));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Post>> updatePost(@PathVariable String id, @RequestBody Post post) {
        return postService.updatePost(id, post)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Object>> deletePost(@PathVariable String id) {
        return postService.deletePost(id)
                .then(Mono.just(ResponseEntity.noContent().build()))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
