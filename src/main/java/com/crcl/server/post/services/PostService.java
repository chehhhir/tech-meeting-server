package com.crcl.server.post.service;

import com.crcl.server.post.domain.Post;
import com.crcl.server.post.repository.PostRepository;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class PostService {

    private final PostRepository postRepository;

    @Autowired
    public PostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public Mono<Post> createPost(Post post) {
        return postRepository.save(post);
    }

    public Mono<Post> getPostById(String id) {
        return postRepository.findById(id);
    }

    public Mono<Page<Post>> getAllPosts(Pageable pageable) {
        return postRepository.findAll(pageable);
    }

    public Mono<Post> updatePost(String id, Post post) {
        return postRepository.findById(id)
                .flatMap(existingPost -> {
                    existingPost.setTitle(post.getTitle());
                    existingPost.setContent(post.getContent());
                    return postRepository.save(existingPost);
                });
    }

    public Mono<Void> deletePost(String id) {
        return postRepository.deleteById(id);
    }
}

