package com.crcl.server.post.domain;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
@Data
@NoArgsConstructor
public class Post {
    @Id
    private String id;
    private String title;
    private String content;
    private String image;
    @CreatedDate
    private LocalDateTime createDate;

    public Post(String title, String content, String image) {
        this.title = title;
        this.content = content;
        this.image = image;
    }
}
