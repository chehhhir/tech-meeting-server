package com.crcl.server.post.repository;

import com.crcl.server.post.domain.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ReactivePageableRepository {

    Mono<Page<Post>> findAll(Pageable pageable);
}
