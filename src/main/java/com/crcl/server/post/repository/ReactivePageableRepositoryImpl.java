package com.crcl.server.post.repository;

import com.crcl.server.post.domain.Post;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
@RequiredArgsConstructor
public class ReactivePageableRepositoryImpl implements ReactivePageableRepository {
    private final ReactiveMongoTemplate reactiveMongoTemplate;

    @Override
    public Mono<Page<Post>> findAll(Pageable pageable) {
        final Query query = new Query().with(pageable);

        return reactiveMongoTemplate.find(query, Post.class)
                .collectList()
                .map(posts -> new PageImpl<>(posts, pageable, posts.size()));
    }
}
