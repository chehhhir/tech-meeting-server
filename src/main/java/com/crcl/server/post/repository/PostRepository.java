package com.crcl.server.post.repository;

import com.crcl.server.post.domain.Post;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface PostRepository extends ReactiveMongoRepository<Post, String> , ReactivePageableRepository{
    @Override
    Flux<Post> findAll();
}
